package com.mircroservice.department.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mircroservice.department.entity.Department;
import com.mircroservice.department.repo.DepartmentRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DepartmentService {

	@Autowired
	private DepartmentRepository departmentRepository;

	public Department saveDep(Department department) {
		log.info("name"+department.getDepartmentName());
		return departmentRepository.save(department);
	}

	public Department getById(Long depId) {
		return departmentRepository.findByDepartmentId(depId);
	}
}
