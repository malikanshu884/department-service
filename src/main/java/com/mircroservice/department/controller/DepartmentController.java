package com.mircroservice.department.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mircroservice.department.entity.Department;
import com.mircroservice.department.service.DepartmentService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class DepartmentController {

	@Autowired
	private DepartmentService departmentService;
	
	@PostMapping("/")
	public Department saveDep(@RequestBody Department department) {
		log.info("inside post method" );
		log.info("name"+department.getDepartmentName());
		return departmentService.saveDep(department);
	}
	
	@GetMapping("/department/{depId}")
	public Department findByDepartmentId(@PathVariable Long depId) {
		return departmentService.getById(depId);
	}
	
}
